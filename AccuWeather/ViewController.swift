//
//  ViewController.swift
//  AccuWeather
//
//  Created by Hammas Naik on 09/05/2019.
//  Copyright © 2019 Hammas Naik. All rights reserved.
//

import UIKit
import MaterialComponents
import Alamofire
import MaterialComponents.MaterialButtons_ColorThemer

class ViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource
{
    
    
    var CityofCountry = [String]()
    var CountryofCity = [String]()
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var WeatherTextField: MDCTextField!
    
    @IBOutlet weak var Select: MDCButton!
    
    var weathertextfield:MDCTextInputControllerFilled?
    
    
    let colorScheme = MDCButtonScheme()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableview.isHidden = true
        
        WeatherTextField.delegate = self
        weathertextfield = MDCTextInputControllerFilled(textInput: WeatherTextField)
        weathertextfield?.borderFillColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1) //background color
        weathertextfield?.placeholderText = "Choose Location"
        weathertextfield?.inlinePlaceholderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        weathertextfield?.floatingPlaceholderScale = 1.1
        weathertextfield?.floatingPlaceholderNormalColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        weathertextfield?.floatingPlaceholderActiveColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        WeatherTextField.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        weathertextfield?.activeColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)  // for underline color
        weathertextfield?.underlineHeightActive = 3 // Underline height
        weathertextfield?.textInputClearButtonTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //Button
        
        Select.setBorderColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        Select.setBorderWidth(2, for: .normal)
        Select.setBackgroundColor(UIColor.clear, for: .normal)
        
        Select.setBackgroundColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .selected)
        Select.setBorderColor(#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), for: .selected)
        Select.setBorderWidth(2, for: .selected)
        
    }
    
    
    @IBAction func Selectcity(_ sender: MDCButton)
    {
        getData()
    }
    
    
    
    
    
    func getData()
    {
        let urlString = "http://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey=N1lZBUDmU8q9Z0G9tzoeINVfcGfCxuQA&q=\(WeatherTextField.text!)&language=en-us"
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON
            {
                response in
                print(response.request as Any)
                print(response.response as Any)
                print(response.data as Any)
                print(response.result as Any)
                
                switch response.result
                {
                case .success:
                    if let JSON = response.result.value
                    {
                        let dlc = (JSON as! NSArray)
                        print(dlc)
                        let city = dlc.value(forKey: "AdministrativeArea") as! NSArray
                        for C in city
                        {
                            let names = C as! NSDictionary
                            self.CityofCountry.append(names.value(forKey: "LocalizedName") as! String)
                        }
                        
                        
                        let countrys = dlc.value(forKey: "Country") as! NSArray
                        
                        
                        for country in countrys
                        {
                            let names = country as! NSDictionary
                            self.CountryofCity.append(names.value(forKey: "LocalizedName") as! String)
                        }
                        self.tableview.reloadData()
                        self.tableview.isHidden = false
                    }
                case .failure(let error):
                    print(error)
                }
        }
    }
}

extension ViewController
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CityofCountry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = CityofCountry[indexPath.row] + "," + CountryofCity[indexPath.row]
        return cell
    }
}


class

